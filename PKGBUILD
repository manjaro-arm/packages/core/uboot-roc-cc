# U-Boot: Roc-CC
# Maintainer: Dan Johansen <strit@manjaro.org>
# Contributor: Kevin Mihelich <kevin@archlinuxarm.org>

pkgname=uboot-roc-cc
pkgver=2023.01
pkgrel=1
pkgdesc="U-Boot for Firefly Roc-CC"
arch=('aarch64')
url='http://www.denx.de/wiki/U-Boot/WebHome'
license=('GPL')
makedepends=('bc' 'dtc' 'python-setuptools' 'swig')
provides=('uboot')
conflicts=('uboot')
install=${pkgname}.install
_tfver=2.8
source=("https://gitlab.denx.de/u-boot/u-boot/-/archive/v${pkgver/rc/-rc}/u-boot-v${pkgver/rc/-rc}.tar.bz2"
        "https://git.trustedfirmware.org/TF-A/trusted-firmware-a.git/snapshot/trusted-firmware-a-$_tfver.tar.gz"
        "0001-mmc-sdhci-allow-disabling-sdma-in-spl.patch")
md5sums=('f4147f7f1b85a4cd410e75b4c27ed181'
         '9671bd2898aa11e57c3b573252a10920'
         'dbd251d5c68c14825483da2fa2d5898b')

prepare() {
  cd u-boot-v${pkgver/rc/-rc}
  patch -Np1 -i "${srcdir}/0001-mmc-sdhci-allow-disabling-sdma-in-spl.patch"
}

build() {
  # Avoid build warnings by editing a .config option in place instead of
  # appending an option to .config, if an option is already present
  update_config() {
    if ! grep -q "^$1=$2$" .config; then
      if grep -q "^# $1 is not set$" .config; then
        sed -i -e "s/^# $1 is not set$/$1=$2/g" .config
      elif grep -q "^$1=" .config; then
        sed -i -e "s/^$1=.*/$1=$2/g" .config
      else
        echo "$1=$2" >> .config
      fi
    fi
  }

  unset CFLAGS CXXFLAGS CPPFLAGS LDFLAGS
  
  cd trusted-firmware-a-$_tfver
  
  echo -e "\nBuilding TF-A for Firefly Roc-CC...\n"
  make PLAT=rk3328
  cp build/rk3328/release/bl31/bl31.elf ../u-boot-v${pkgver/rc/-rc}/
  
  cd ../u-boot-v${pkgver/rc/-rc}
  
  echo -e "\nBuilding U-Boot for Firefly Roc-CC...\n"
  make roc-cc-rk3328_defconfig
  update_config 'CONFIG_IDENT_STRING' '" Manjaro Linux ARM"'
  update_config 'CONFIG_SPL_MMC_SDHCI_SDMA' 'n'
  update_config 'CONFIG_MMC_HS400_SUPPORT' 'y'
  make EXTRAVERSION=-${pkgrel}
}

package() {
  cd u-boot-v${pkgver/rc/-rc}

  mkdir -p "${pkgdir}/boot/extlinux"

  install -D -m 0644 u-boot.itb idbloader.img -t "${pkgdir}/boot"
}
